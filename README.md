# nsupdate_bash

- IPv6 only
- nsupdate only

## Getting started

- Have a keyfile for nsupdate
- Download the the script
- Run it to learn the options
- embed it into cron or better systemd.timer

## Authors and acknowledgment
Jochen Demmer
https://wiki.junicast.de
https://libcom.de

## License
BSD

## Project status
active
