#!/usr/bin/env bash
# This script updates DNS records via nsupdate
# RFC 2136
# It only does IPv6 so far, IPv4 is legacy crap
# Jochen Demmer
# Libcom.de

# Requirements:
# wget
# dig
# nsupdate

# Initialize variables
SERVER=""
HOSTNAME=""
DEBUG=""
ZONE=""
TTL=""
KEYFILE=""
TIMEOUT=""

# Function to display help text
show_help() {
    echo "Usage: $0 -s SERVER -h HOSTNAME -z ZONE -k KEYFILE -t TTL -o TIMEOUT"
    echo "-s is the server the nsupdate request will be sent to."
    echo "-h is the fully qualified hostname that shall be updated."
    echo "-z is the zone the host under -h is in."
    echo "-k is the absolute path of the keyfile."
    echo "-t is the TTL for the new record set."
    echo "-o is the timeout to wait fetching the current IP of the host."
    echo "-g is a string to grep for in the AAAA record. A record containing that string will not be deleted or modified."
    echo "Mandatory options: -s, -z, -h, -k -t"
}

delete_record() {
    local DELETEIP=$1
    if [ ! -r $KEYFILE ]; then
        logger -p "user.err" -t nsupdate_bash "Keyfile does not exist, cannot delete record."
	return 11
    fi
    $ECHO "server $SERVER" > /tmp/nsupdate_delete
    $ECHO "debug yes" >> /tmp/nsupdate_delete
    $ECHO "zone ${ZONE}." >> /tmp/nsupdate_delete
    $ECHO "update delete ${HOSTNAME} AAAA $DELETEIP" >> /tmp/nsupdate_delete
    $ECHO "send" >> /tmp/nsupdate_delete
    
    $NSUPDATE -k $KEYFILE /tmp/nsupdate_delete 2>&1
    if [ $? -eq 0 ]; then
        logger -t nsupdate_bash "Deletion of record $DELETEIP successfull."
    else
        logger -p "user.err" -t nsupdate_bash "Deletion of record $DELETEIP failed."
	return 10
    fi
}

set_record() {
    local SETIP=$1
    if [ ! -r $KEYFILE ]; then
        logger -t nsupdate_bash "Keyfile does not exist, cannot create record."
	return 11
    fi
    $ECHO "server $SERVER" > /tmp/nsupdate_set
    $ECHO "debug yes" >> /tmp/nsupdate_set
    $ECHO "zone ${ZONE}." >> /tmp/nsupdate_set
    $ECHO "update add ${HOSTNAME} $TTL AAAA ${SETIP}" >> /tmp/nsupdate_set
    $ECHO "send" >> /tmp/nsupdate_set
    
    $NSUPDATE -k $KEYFILE /tmp/nsupdate_set 2>&1
    if [ $? -eq 0 ]; then
        logger -t nsupdate_bash "Setting IP $SETIP successfull."
    else
        logger -p "user.err" -t nsupdate_bash "Setting IP $SETIP failed."
    fi
}

# checking for commands
# echo will always be there, right?
ECHO=$(which echo)
if ! command -v wget &> /dev/null; then
    echo "Error: 'wget' is not installed on this system. Please install wget before running this script."
    exit 1
else
    WGET=$(which wget)
fi
if ! command -v nsupdate  &> /dev/null; then
    echo "Error: 'nsupdate' is not installed on this system. Please install wget before running this script."
    exit 1
else
    NSUPDATE=$(which nsupdate)
fi
if ! command -v dig  &> /dev/null; then
    echo "Error: 'dig' is not installed on this system. Please install wget before running this script."
    exit 1
else
    DIG=$(which dig)
fi

# Use getopt to parse command-line options
OPTS=$(getopt -o s:h:z:k:t:o:g: -n "$0" -- "$@")
if [ $? != 0 ]; then
    show_help
    exit 1
fi

# Extract options and their arguments into variables
while getopts "s:h:z:k:t:o:g:" OPTION; do
    case $OPTION in
        s) SERVER="$OPTARG"
	    ;;
        h) HOSTNAME="$OPTARG"
            ;;
        z) ZONE="$OPTARG"
            ;;
        k) KEYFILE="$OPTARG"
            ;;
        t) TTL="$OPTARG"
            ;;
	o) TIMEOUT="$OPTARG"
            ;;
	g) GREPSTRING="$OPTARG"
            ;;
        *) echo "Error"
           show_help
           exit 1
           ;;
    esac
done

# Check for mandatory options
if [ -z "$SERVER" ] || [ -z "$HOSTNAME" ] || [ -z "$ZONE" ] || [ -z "$KEYFILE" ] || [ -z "$TTL" ]; then
    echo "Mandatory options are missing!"
    show_help
    exit 1
fi

if [ -z "$TIMEOUT" ]; then
    TIMEOUT=10
fi

IP6=$($WGET -t $TIMEOUT -q -O - http://ip6only.me/api/ | awk -F',' '{print $2}')
CURRENTRECORD=($($DIG +short -t AAAA $HOSTNAME))
logger -t nsupdate_bash "Working on record $HOSTNAME."

if [ ${#CURRENTRECORD[@]} -eq 0 ]; then
    logger -t nsupdate_bash "No DNS record found for this DNS name ${HOSTNAME}."
    logger -t nsupdate_bash "Creating record with IP ${IP6}."
    set_record "${IP6}"
else
    for record in "${CURRENTRECORD[@]}"; do
        if [ "$record" == "${IP6}" ]; then
            logger -t nsupdate_bash "The record $record in DNS is the same as the hosts current IP ${IP6}. No action."
	    continue
        elif [[ ! -z $GREPSTRING ]] && [[ $(echo $record | grep -c $GREPSTRING) -ge 1 ]]; then
               logger -t nsupdate_bash "The record $record in DNS matches the grepstring $GREPSTRING. It will not be modified. Nothing to do here."
	       continue
	else
	    logger -t nsupdate_bash "Trying to delete old record $record and recreate it with the IP ${IP6}."
            delete_record $record
            set_record "${IP6}"
	fi
    done
fi

